#ifndef _DATE_H_
#define _DATE_H

#include <ostream>

class Date 
{
    int _day, _month, _year;
    
    // Our friends
    friend std::ostream& operator<< (std::ostream& os, const Date& d);

public:
    // Constructors!
    Date(int day = 0, int month = 0, int year = 0);
     
    // Copy ctor
    Date(const Date& rvalue);
    
    // Type conversion ctor: const char* => Date
    /*explicit*/ Date(const char* rep);
    
    // Accessor member functions 
    int& day();
    Date& day(int day);
    
    // Operations
    void print() const;
};

std::ostream& operator<< (std::ostream& os, const Date& d);

#endif
