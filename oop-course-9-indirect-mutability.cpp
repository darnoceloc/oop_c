#include <iostream>
#include <string>
#include <sstream>

using namespace std;

class Cache 
{
    friend class Date;
    
    string cache;
    bool validCache;
public:
    Cache();
};

Cache::Cache()
    : cache(""), validCache(false)
{
}

class Date 
{
     short day, month, year;
     Cache* c; // to be initialized in constructor(s)
 
    string compute_new_string_representation() const;
 
public:
    Date(short d, short m, short y);
    ~Date();

    // const member function
    const char* toString() const;
};

Date::Date(short d, short m, short y)
{
    day = d;
    month = m;
    year = y;
    
    c = new Cache;
}

Date::~Date()
{
    if (c != nullptr)
    {
        delete c;
    }
}

const char* Date::toString() const 
{
    if(!c->validCache) 
    {
        c->cache = compute_new_string_representation(); 
        c->validCache = true;
    }
    return c->cache.c_str();
}


string Date::compute_new_string_representation() const
{
    return to_string(day) + "/" + to_string(month) + "/" + to_string(year);
    
    // char buffer[10];
    // sprintf(buffer, "%d/%d/%d", day, month, year);
    // return buffer;
    
    // stringstream  x;
    // x << day << "/" << month << "/" << year;
    // return x.str();
}

int main()
{
    Date d{20, 04, 2021};
    
    cout << d.toString();

    return 0;
}
