#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <functional>
#include <string.h>

using namespace std;

class Person
{
    char* name_;
    vector<Person*> contacts_;
    bool infected_; // = false
    bool vaccinated_; // = false
    
    public:
        Person(const char* n)
            : infected_{false}, 
              vaccinated_{false},
              name_{new char [strlen(n)+1]}
        {
            strcpy(name_, n);
        }
        
        ~Person()
        {
            delete [] name_;
        }
        
        bool infected() const
        {
            return infected_;
        }
        
        Person& setInfected()
        {
            infected_ = true;
            return *this;
        }
        
        bool vaccinated() const
        {
            return vaccinated_;
        }
        
        Person& setVaccinated()
        {
            vaccinated_ = true;
            return *this;
        }
        
        void addContacts(vector<Person*> newContacts);
		
		const vector<Person*> contacts() const
        {
            return contacts_;
        }
        
        friend ostream& operator<<(ostream& os, const Person& p);
        
        static void spreadDisease(Person* person);
};

void Person::addContacts(vector<Person*> newContacts)
{
    for(auto c : newContacts)
    {
        contacts_.push_back(c);
        c->contacts_.push_back(this);
    }
}

ostream& operator<<(ostream& os, const Person& p)
{
    os << p.name_;
}

void Person::spreadDisease(Person* person)
{
    if (!person->infected() && !person->vaccinated())
    {
        person->setInfected();
        
        for(auto c : person->contacts_)
        {
            Person::spreadDisease(c);
        }   
    }
}

int main()
{
    // Setup the world
    vector<Person*> group =
    {
        new Person("P1"),
        new Person("P2"),
        new Person("P3"),
        new Person("P4"),
        new Person("P5"),
        new Person("P6"),
        new Person("P7")
    };
    
    group[0]->addContacts({group[1], group[3], group[6]});
    group[2]->addContacts({group[5]});
    group[4]->addContacts({group[5]});
    
    group[6]->setVaccinated();
    
    // Propagate the disease
    Person::spreadDisease(group[1]);
	
	// Propagate the disease (functional implementation)
	//const std::function<void(Person*)> infect = [&infect](Person* p) 
    //{
    //    if (!p->infected() && !p->vaccinated())
    //    {
    //        p->setInfected();
    //        
    //        for(auto c : p->contacts())
    //        {
    //            infect(c);
    //        }   
    //    }
    //};

    //infect(group[1]);
    
    // Compute the count of infected people
    auto lambdaExpr = [] (Person* p) -> int 
    {
        static int sum = 0;
        sum += p != nullptr ? p->infected() : 0;
        return sum;
    };

    for_each(group.begin(), 
			 group.end(), 
			 lambdaExpr);

    cout << "Total infected people = " 
         << lambdaExpr(nullptr) 
         << endl;
         
    cout << "Infection coverage = " 
         << setprecision(2) 
         << ((float)lambdaExpr(nullptr) / group.size() * 100) 
         << "%" 
         << endl;
    
    
    // Display infected people
    cout << "Infected people: ";
    for_each(group.begin(), 
             group.end(), 
             [](auto p) 
             { 
                 if (p->infected()) 
                 { 
                     cout << *p << ", "; 
                 }
             });


    // Destroy the world
    for_each(group.begin(), 
             group.end(), 
             [](auto p) 
             { 
                 delete p; 
             });

    return 0;
}
