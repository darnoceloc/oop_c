#include <iostream>

using namespace std;

int vizitat[100],n,a[100][100]; //matricea, nr de persoane si vectorul vizitat initializate cu 0 (variabile globale)

void matrice (int a[100][100], int n)
{
   int i,j;

    while(n-- > 0) //citirea persoanelor care au intrat in contact
    {
        int i, j;
        cin >> i >> j;
        a[i][j]=a[j][i]=1;
    }
}

void df(int x)
{
    vizitat[x]=1; 

    int i;

    for(i=1;i<=n;i++)
        if(a[x][i]==1&&vizitat[i]==0)
          df(i);
}

int main()
{
    int i,j,p,nr_inf=0,np;

    cin>>n; //se citeste numarul de persoane
    
    cin>>np;

    matrice(a, np); // matricea de adiacenta a grafului persoanelor

    cin>>p;

    df(p); //parcurgerea in adancime din nodul p

    for(i=1;i<=n;i++)
        if(vizitat[i]==1) 
            nr_inf++;

    cout<<"numarul de infectati este "<<nr_inf;

    return 0;
}