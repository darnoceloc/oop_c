#include <iostream>

using namespace std;

template <class TElement> class vector
{
     TElement *v;
     int size;
public:
    class BadIndex {};

     vector( ): v{nullptr}, size{0}
     {
     }

     TElement get(int n)
     {
          if (0<=n && n<size)
                 return v[n];
          throw BadIndex{};
     }

     void  add(TElement x);
};

template<class TElement> void vector< TElement >::add(TElement x)
{
    cout << "vector<T>::add" << endl;
    cout << x << endl;
    // add e to the vector v
}

// specialization for void*
template<> class vector<void*>
{
     void** p;

public:
    class BadIndex {};

     vector( ): p{nullptr}
     {
     }

     void* get(int n)
     {
         cout << "vector<void*>::get" << endl;
         return nullptr;
     }

     void  add(void* x)
     {
        cout << "vector<void*>::add" << endl;
     }
};

// partial specialization for T*
template<class T> class vector<T*> : private vector<void*>
{
public:
    typedef vector<void*> Base;
    vector()
        : Base()
    {
    }

    explicit vector(int i)
        : Base(i)
    {
    }

    T* get(int i)
    {
        return static_cast<T*> (Base::get(i));
    }

    void add(T* e)
    {
        Base::add(e);
    }
};


template<class T> T minimum(T x, T y)
{
     return x < y ? x : y;
}

//double minimum(double d, int i)
//{
//    return d < i ? d : i;
//}

template<class S> class complex
{
private:
      S re_, im_;
public:
    complex()
    {
    }

//    complex(const complex<S>& c)
//        : re_(c.re_), im_(c.im_)
//    {
//    }

    S re() const
    {
        return re_;
    }

    S im() const
    {
        return im_;
    }

    template<class T> complex(const complex<T>& c);
};

template<class S> template<class T> complex<S>::complex(const complex<T>& c)
    : re_(c.re()), im_(c.im())
{
    cout << "complex<S>::complex(const complex<T>& c), S = " << typeid(S).name() << ", T = " << typeid(T).name() << endl;
}

// Test functions
class MyClass
{
};

ostream& operator<< (ostream& os, const MyClass& o)
{
    os << "My class goes here";
    return os;
}

bool operator< (const MyClass& a, const MyClass& b)
{
    return true;
}


void test_vector()
{
    vector<int> v;
    v.add(1) ;
    v.add(100);
    //cout << v.get (0);

    vector<float> fv;
    fv.add(1.2);

    //cout << v.get(0);
}



void test_vector_user_defined_type()
{
    vector<MyClass> v;
    v.add(MyClass{}) ;
}


void test_min()
{
    cout << minimum(0, 1) << endl;

    cout << minimum<int>(0, 1) << endl;

    cout << minimum(1.2, 1.3) << endl;

    // ambiguous call
    // cout << minimum(0.9, 1) << endl;
    // wiht int
    cout << minium<int>(0.9, 1) << endl;
    // with double
    cout << minimum<double>(0.9, 1) << endl;

    MyClass x, y;

    cout << minimum(x, y) << endl; // => MyClass minimum(MyClass x, MyClass y) { return x < y ? x : y; }
}

void test_specialisation()
{
    vector<void*> v0;
    v0.add(new int{1});
    v0.add(new MyClass);

    vector<int*> vi;

    vi.add(new int{1});
    //vi.add(new MyClass);
    vi.get(10);
}

void test_complex()
{
    complex<float> cf;
    complex<float> cf2 = cf;
    // conversion from complex<float> to complex<double> not possible
    complex<double> cd = cf; // use float -> double conv.
}

int main()
{
    test_vector();

    //test_vector_user_defined_type();

    //test_min();

    //test_specialisation();

    // test_complex();

    return 0;
}
