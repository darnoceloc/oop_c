#include <iostream>
#include <vector>
#include <algorithm>
#include <string.h>

using namespace std;

class Person
{
    bool infected_, vaccinated_;
    char* name_;
    vector<Person*> contacts_;
    
public:
    Person(const char* name);
    ~Person();
    
    void addContacts(vector<Person*> newContacts);
    
    bool infected() const;
    
    void setInfected();
    
    void setVaccinated();
    
    // const char* name() const;
    
    static void spreadDisease(Person* p);
    
    friend ostream& operator<<(ostream& os, const Person& p);
};

inline Person::Person(const char* name)
    // initialization list
    : infected_{false},
      vaccinated_{false},
      name_{new char [strlen(name) + 1]}
{
    strcpy(name_, name);
}

inline Person::~Person()
{
    delete [] name_;
}

void Person::addContacts(vector<Person*> newContacts)
{
    for(const auto person : newContacts)
    {
        contacts_.push_back(person);
        person->contacts_.push_back(this);
    }
}

inline bool Person::infected()  const
{
    return infected_;
}

inline void Person::setVaccinated()
{
    vaccinated_ = true;
}

inline void Person::setInfected()
{
    infected_ = true;
}

// inline const char* Person::name() const
// {
//     return name_;
// }

void Person::spreadDisease(Person* p)
{
    if (!p->infected_ && !p->vaccinated_)
    {
        p->infected_ = true;
        
        for(auto contact : p->contacts_)
        {
            spreadDisease(contact);
        }
    }    
}

ostream& operator<<(ostream& os, const Person& p)
{
    os << p.name_;
    return os;
}

int main()
{
    // Setup the world
    vector<Person*> group = 
    {
        new Person("P1"),
        new Person("P2"),
        new Person("P3"),
        new Person("P4"),
        new Person("P5"),
        new Person("P6"),
        new Person("P7")
    };
    
    group[0]->addContacts({group[1], group[3], group[6]});
    group[2]->addContacts({group[5]});
    group[4]->addContacts({group[5]});
    
    // Spread the disease
    group[3]->setVaccinated();
    
    Person::spreadDisease(group[1]);
    
    // Count the number of infected people in the group
    int infectedCount = 0;
    
    for(const auto person : group)
    {
        infectedCount += person->infected();
    }
    
    cout << "Number of infected people = " << infectedCount << endl;
    
    // Display infected people
    cout << "Infected people: ";
    for_each(group.begin(),
             group.end(),
             [](auto person) 
             {
                 if (person->infected())
                 {
                     cout << *person << ", ";
                 }
             } );
    
    return 0;
}
