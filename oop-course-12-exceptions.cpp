#include <iostream>
#include <string>
#include <vector>

using namespace std;

// Custom exception classes
class FileNotFoundException {
public:
    string describe() const { return "File not found exc."; }
} ;

class FileWriteException {
public:
    string describe() const { return "File write exc."; }
} ;


// SmartFile class
class File {
    FILE* handle;
    string name;
public:
    File(const char* name, const char* mode);
    File(const File&) = delete; // forbid copy ctor
    void write(const char* data, int size);
    ~File();
};

File::File(const char* n, const char* mode)
    : name{n}, handle{fopen(n, mode)} {
    if (handle==nullptr) {
        throw FileNotFoundException { };
    }

    cout << "File::File(" << name << ", " << mode << ")" << endl;
}

File::~File() {
    if (handle!=nullptr) {
        fclose(handle);
    }

    cout << "File::~File()" << endl;
}

void File::write(const char* data, int size) {
     size_t written = fwrite(data, 1, size, handle);
     if (written!=size)
          throw FileWriteException { };
}

// try-catch in initialization list
class X  {
    vector<int> v;
public:
     X(int size);
};

X::X(int size)
try
   : v(size) // initialize v by size
{
    // body of X::X(int) ctor
}
catch(exception) {
    cerr << "Bad size" << endl;
    throw;
}

// Tests
void test_Write_To_SmartFile() {
    try {
        string test = "Write test in my sample file.";
        File f("sample.txt", "w+");
        f.write(test.c_str(), test.length());
        cout << "File updated successfully." << endl;
    }
    catch(FileNotFoundException e) {
        cerr << e.describe();
    }
    catch(FileWriteException e) {
        cerr << e.describe();
    }
}

void test_When_Exception_In_InitList() {

    try {
        X x{-1};
    }
    catch(exception e) {
        cerr << "End it.";
    }

}

int main(int, char*[]) {

    test_Write_To_SmartFile();

    // test_When_Exception_In_InitList();
}
